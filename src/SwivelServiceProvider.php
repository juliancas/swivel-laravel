<?php

namespace Zumba\Swivel;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;

class SwivelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath($raw = __DIR__ . '/../config/swivel.php') ?: $raw;
        $this->publishes([
            $source => config_path('swivel.php'),
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Manager::class, function () {
            // Get this value from the session or from persistent storage.
            $bucketFromSession = Request::session()->get('bucket');
            $userBucket = !empty($bucketFromSession) ? $bucketFromSession : config('swivel.default_bucket');;

            // Get the feature map data from persistent storage.
            $mapData = config('swivel.features');;

            // Make a new configuration object
            $config = new \Zumba\Swivel\Config($mapData, $userBucket);
            return new Manager($config);
        });
        $this->app->alias(Manager::class, 'zumba-swivel');
    }
}
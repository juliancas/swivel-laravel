<?php
namespace Zumba\Swivel;
use Illuminate\Support\Facades\Facade;
class SwivelFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'zumba-swivel';
    }
}